import { MongoClient, ObjectID } from 'mongodb';
// const MongoClient = require('mongodb').MongoClient

function circulationRepo() {

    const url = 'mongodb://localhost:27017';
    const dbName = 'circulation';

    function remove(id) {
        return new Promise(async(resolve, reject) =>{
            const client = new MongoClient(url);
            try {
                await client.connect();
                const db = client.db(dbName);
                const removedItem = db.collection('newspapers').deleteOne({_id: ObjectID(id)});
                resolve(removedItem.deletedCount == 1);
                client.close();
            } catch (error) {
                reject(error);
            }
        }
    

    function update(id, newItem) {
        return new Promise(async(resolve, reject) =>{
            const client = new MongoClient(url);
            try {
                await client.connect();
                const db = client.db(dbName);
                const updatedItem = db.collection('newspapers')
                .findOneAndReplace({_id: ObjectID(id)}, newItem);
            resolve(updatedItem.value);
            } catch (error) {
                reject(error);
            }        })
    }

    function get(query, limit) {
        return new Promise(async(resolve, reject) =>{
            const client = new MongoClient(url);
            try {
                await client.connect();
                const db = client.db(dbName);

                let items = db.collection('newspapers').find(query);

                if (limit>0) {
                    items = items.limit(limit);
                }

                resolve(await items.toArray());
                client.close();
            } catch (error) {
                reject(error)
            }
        })
    }

    function getById(id) {
        return new Promise(async (Resolve, Reject) => {
            const client = new MongoClient(url);
            try {
                await client.connect();
                const db = client.db(dbName);
                const item = await db.collection('newspapers').findOne({_id: ObjectID(id)});
                Resolve(item);
                client.close();
            } catch (error) {
                Reject(error);
            }
        })
    }

    function add(item) {
        return new Promise(async(Resolve, Reject) =>{
            const client = new MongoClient(url);
            try {
                await client.connect();
                const db = client.db(dbName);
                const addedItem = await db.collection('newspapers').insertOne(item);
                console.log(addedItem.ops[0]);
                Resolve(addedItem);
                client.close();
            } catch (error) {
                Reject(error);
            }
        })
    }

    function loadData(data) {
        return new Promise(async(resolve, reject) =>{
            const client = new MongoClient(url);
            try {
                await client.connect();
                const db = client.db(dbName);

                results = await db.collection('newspapers').insertMany(data);
                resolve(results);
                client.close();

            } catch (error) {
                reject(error);
            }
        })
    }
    return { loadData, get, getById, add, update, remove }

}

module.exports = circulationRepo();

// const { MongoClient } = require('mongodb');

// function circulationRepo() {
//   const url = 'mongodb://localhost:27017';
//   const dbName = 'circulation';

//   function loadData(data) {
//     return new Promise(async (resolve, reject) => {
//       const client = new MongoClient(url);
//       try {
//         await client.connect();
//         const db = client.db(dbName);

//         results = await db.collection('newspapers').insertMany(data);
//         resolve(results);

//       } catch (error) {
//         reject(error)
//       }
//     })
//   }

//   return { loadData }

// }

// module.exports = circulationRepo();