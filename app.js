const MongoClient = require('mongodb').MongoClient;
const redis = require("redis");
const assert = require('assert');
const http = require('http');
const fs = require('fs');
var express=require("express"); 
var bodyParser=require("body-parser"); 
async function main(){

    // const mongoose = require('mongoose'); 
    // mongoose.connect('mongodb://localhost:27017/circulation'); 
    // var db=mongoose.connection; 
    // db.on('error', console.log.bind(console, "connection error")); 
    // db.once('open', function(callback){ 
    //     console.log("connection succeeded"); 
    // }) 
  
const REDIS_PORT = process.env.PORT || 6379;
const redisclient = redis.createClient();
redisclient.on("error", function(error) {
    console.error(error);
  });
redisclient.set("key", "value", redis.print);
redisclient.get("key", redis.print);

var app=express() 
var router  = express.Router();
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(bodyParser.json()); 
app.use(express.static('public')); 
app.use(bodyParser.urlencoded({ 
    extended: true
})); 

var Newspaper;
var Daily2004;
var Daily2013;
var changeDC;
var PPrize1;
var PPrize2;
var PPrize3;

app.post('/sign_up', async function(req,res){ 
    Newspaper = req.body.Newspaper; 
    Daily2004 =req.body.Daily2004; 
    Daily2013 = req.body.Daily2013; 
    changeDC =req.body.changeDC; 
    PPrize1 =req.body.PPrize1; 
    PPrize2 =req.body.PPrize2; 
    PPrize3 =req.body.PPrize3; 
    // var name = req.body.name; 
    // var email =req.body.email; 
    // var pass = req.body.password; 
    // var phone =req.body.phone; 
    var data = { 
        "Newspaper": Newspaper, 
        "Daily2004":Daily2004, 
        "Daily2013":Daily2013, 
        "changeDC":changeDC,
        "PPrize1":PPrize1,
        "PPrize2":PPrize2,
        "PPrize3":PPrize3
    } 
    console.log(data);
    redisclient.set(Newspaper.value, Daily2004.value, redis.print);
    redisclient.get(Newspaper.value, function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }
        console.log('GET result ->' + result);
    });
    redisclient.expire(Newspaper.value, 3000);
    const addedItem = await circulationRepo.add(data);
    addedItem.assert(_id);
    const addedItemQuery = await circulationRepo.getById(addedItem._id);
    assert.deepEqual(addedItemQuery, newItem);
    client.close();
    // var data = { 
    //     "name": name, 
    //     "email":email, 
    //     "password":pass, 
    //     "phone":phone 
    // } 
    console.log(Newspaper);
    console.log("hahah finally!");
    // console.log(data);
    console.log("Printing");
    return res.redirect('signup_success.html'); 
}) 
// console.log(data)
console.log("outside too")
console.log(Newspaper);

app.get('/',function(req,res){ 
    res.set({ 
        'Access-control-Allow-Origin': '*'
        }); 
    return res.redirect('index.html'); 
    }).listen(3000) 



const circulationRepo = require('./repos/circulationRepo');
const data = require('./circulation.json');
const url = 'mongodb://localhost:27017';
const dbName = 'circulation';
const client = new MongoClient(url);
await client.connect();

try{
const results = await circulationRepo.loadData(data);
console.log(results.insertedCount, results.ops);
assert.equal(data.length, results.insertedCount);

const admin = client.db(dbName).admin();

const getData = await circulationRepo.get();
assert.equal(data.length, getData.length);

const filterData = await circulationRepo.get({Newspaper: getData[4].Newspaper});
assert.deepEqual(filterData[0], getData[4]);

const limitData = await circulationRepo.get({}, 3);
assert.equal(limitData.length, 3);

// const Id = getData._id.toString();
const byId = await circulationRepo.getById(getData[4]._id);
assert.deepEqual(byId, getData[4]);

const updatedItem = await circulationRepo.update(addedItem._id, data);
const newUpdatedItemQuery = await circulationRepo.getById(addedItem._id);
assert.equal(newUpdatedItemQuery.Newspaper, "new data");

const removed = await circulationRepo.remove(addedItem._id);
assert(removed);


const newItem = {
    "Newspaper": Newspaper,
    "Daily Circulation, 2004": Daily2004,
    "Daily Circulation, 2013": Daily2013,
    "Change in Daily Circulation, 2004-2013": changeDC,
    "Pulitzer Prize Winners and Finalists, 1990-2003": PPrize1,
    "Pulitzer Prize Winners and Finalists, 2004-2014": PPrize2,
    "Pulitzer Prize Winners and Finalists, 1990-2014": PPrize3
}
const addedItem = await circulationRepo.add(newItem);

} catch (error) {
    console.log(error)
} finally {
await client.db(dbName).dropDatabase();
console.log(await admin.listDatabases());
client.close();
}

console.log("server listening at port 3000"); 
}

main()